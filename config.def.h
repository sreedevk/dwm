/* See LICENSE file for copyright and license details. */
#include "themes/kanagawa.h"
#include <X11/X.h>
#include <X11/XF86keysym.h>
#include <stdlib.h>

/* appearance */
static const unsigned int borderpx = 3; /* border pixel of windows */
static const unsigned int snap     = 32;    /* snap pixel */

/* bar configuration */
static const int showbar     = 1;  /* 0 means no bar */
static const int topbar      = 1;   /* 0 means bottom bar */
static const int vertpad     = 10; /* vertical padding of bar */
static const int sidepad     = 10; /* horizontal padding of bar */
static const int user_bh     = 28; /* 0 means that dwm will calculate bar height */
static const int horizpadbar = 10; /* horizontal padding for statusbar */
static const int vertpadbar  = 0;   /* vertical padding for statusbar */

/* gap configuration */
static const unsigned int gappih = 10; /* horiz inner gap between windows */
static const unsigned int gappiv = 10; /* vert inner gap between windows */
static const unsigned int gappoh = 10; /* horiz outer gap between windows and screen edge */
static const unsigned int gappov = 10; /* vert outer gap between windows and screen edge */
static int smartgaps             = 0; /* 1 means no outer gap when there is only one window */

/* focus configuration */
static const int focusonwheel = 0;

static const char *fonts[]    = { "Iosevka Nerd Font:Medium:pixelsize=15:antialias=true" };
static const char dmenufont[] = "Iosevka Nerd Font:Regular:pixelsize=8:antialias=true"; 

static char normbgcolor[]     = "#24273a";
static char normbordercolor[] = "#24273a";
static char normfgcolor[]     = "#cad3f5";
static char selfgcolor[]      = "#24273a";
static char selbordercolor[]  = "#f5a97f";
static char selbgcolor[]      = "#c6a0f6";

static const char *colors[][5] = {
    /*               fg             bg              border   */
    [SchemeNorm] = {normfgcolor,    normbgcolor,    normbordercolor },
    [SchemeSel]  = {selfgcolor,     selbgcolor,     selbordercolor  },
    [SchemeAct]  = {col_foreground, col_background, col_background  },
    [SchemeIdle] = {col_grey,       col_background, col_background  },
};

static const char *tags[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
static const Rule rules[] = {
    /* class          instance    title       tags mask     isfloating   monitor */
    {"firefox",       NULL,       NULL,       1 << 1,       0,           -1       },
    {"pavucontrol",   NULL,       NULL,       0,            1,           -1       },
    {"brave-browser", NULL,       NULL,       1 << 1,       0,           -1       }
};

static const char *const autostart[] = {"dwmblocks", NULL, NULL};

/* layout(s) */
static const float mfact        = 0.55; /* factor of master area size [0.05..0.95]      */
static const int nmaster        = 1;    /* number of clients in master area             */
static const int resizehints    = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1;    /* 1 will force focus on the fullscreen window  */

static const Layout layouts[] = {
    /* symbol     arrange function */
    {  "[]=",     tile                }, /* Default: main on left, rest on right */
    {  "[@]",     spiral              }, /* Fibonacci spiral */
    {  "[\\]",    dwindle             }, /* Decreasing in size right and leftward */
    {  "[M]",     monocle             }, /* All windows on top of eachother */
    {  "><>",     NULL                }, /* no layout function means floating behavior */
    {  NULL,      NULL                },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY, TAG)                                                      \
      {MODKEY,                           KEY, view,       {.ui = 1 << TAG}},   \
      {MODKEY | ControlMask,             KEY, toggleview, {.ui = 1 << TAG}},   \
      {MODKEY | ShiftMask,               KEY, tag,        {.ui = 1 << TAG}},   \
      {MODKEY | ControlMask | ShiftMask, KEY, toggletag,  {.ui = 1 << TAG}},

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char *[]) { "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0";

static const char *dmenucmd[]        = {"dmenu_run", "-m", dmenumon, NULL};
static const char *roficmd[]         = {"rofi", "-show", "drun", NULL};
static const char *rofiswitchcmd[]   = {"rofi", "-show", "window", NULL};
static const char *termcmd[]         = {"alacritty", NULL};
static const char *stcmd[]           = {"st", NULL};
static const char *pavuctrlcmd[]     = {"pavucontrol", NULL};
static const char *xkillcmd[]        = {"xkill", NULL};
static const char maimfullcmd[]      = "maim | xclip -selection clipboard -t image/png";
static const char maimselcmd[]       = "maim --select | xclip -selection clipboard -t image/png";
static const char maimwindowcmd[]    = "maim -i $(xdotool getactivewindow) ~/Pictures/screenshots/$(date +%s).png";
static const char incvolcmd[]        = "pactl set-sink-volume $(pactl info | grep 'Default Sink' | awk '{print $3}') +5%";
static const char decvolcmd[]        = "pactl set-sink-volume $(pactl info | grep 'Default Sink' | awk '{print $3}') -5%";
static const char mutevolcmd[]       = "pactl set-sink-mute $(pactl info | grep 'Default Sink' | awk '{print $3}') toggle";
static const char incbrightcmd[]     = "brightnessctl set +10%";
static const char decbrightcmd[]     = "brightnessctl set 10%-";
static const char screenshotcmd[]    = "maim -s -d 0.2 | xclip -selection clipboard -t image/png";
static const char dunstcloseallcmd[] = "dunstctl close-all";
static const char audioplaycmd[]     = "playerctl play-pause";
static const char audioprevcmd[]     = "playerctl previous";
static const char audionextcmd[]     = "playerctl next";
static const char dunstrestartcmd[]  = "killall dunst && exec notify-send -a dunst 'restart'";
static const char dunstpausecmd[]    = "dunstctl set-paused toggle";
static const char picomcmd[]         = "picom -b";
static const char picomkcmd[]        = "pkill picom";

static const Key keys[] = {
    /* modifier            key                       function        argument */
    {MODKEY,                           XK_d,                     spawn,          {.v = roficmd}          },
    {MODKEY | ControlMask,             XK_d,                     spawn,          {.v = dmenucmd}         },
    {MODKEY,                           XK_Return,                spawn,          {.v = termcmd}          },
    {MODKEY,                           XK_t,                     spawn,          {.v = stcmd}            },
    {MODKEY | ShiftMask,               XK_d,                     spawn,          SHCMD(dunstrestartcmd)  },
    {MODKEY | ControlMask,             XK_s,                     spawn,          SHCMD(maimselcmd)       },
    {MODKEY | ShiftMask,               XK_s,                     spawn,          SHCMD(maimfullcmd)      },
    {MODKEY,                           XK_p,                     spawn,          SHCMD(picomcmd)         },
    {0,                                XK_F7,                    spawn,          SHCMD(picomkcmd)        },
    {0,                                XK_Print,                 spawn,          SHCMD(maimwindowcmd)    },
    {MODKEY,                           XK_a,                     spawn,          {.v = pavuctrlcmd}      },
    {MODKEY,                           XK_period,                focusmon,       {.i = +1}               },
    {MODKEY,                           XK_comma,                 focusmon,       {.i = -1}               },
    {MODKEY | ShiftMask,               XK_period,                tagmon,         {.i = +1}               },
    {MODKEY | ShiftMask,               XK_comma,                 tagmon,         {.i = -1}               },
    {0,                                XF86XK_AudioRaiseVolume,  spawn,          SHCMD(incvolcmd)        },
    {0,                                XF86XK_AudioLowerVolume,  spawn,          SHCMD(decvolcmd)        },
    {0,                                XF86XK_AudioMute,         spawn,          SHCMD(mutevolcmd)       },
    {0,                                XF86XK_MonBrightnessUp,   spawn,          SHCMD(incbrightcmd)     },
    {0,                                XF86XK_MonBrightnessDown, spawn,          SHCMD(decbrightcmd)     },
    {0,                                XF86XK_AudioPlay,         spawn,          SHCMD(audioplaycmd)     },
    {0,                                XF86XK_AudioPrev,         spawn,          SHCMD(audioprevcmd)     },
    {0,                                XF86XK_AudioNext,         spawn,          SHCMD(audionextcmd)     },
    {MODKEY,                           XK_m,                     togglebar,      {0}                     },
    {MODKEY,                           XK_j,                     focusstack,     {.i = +1}               },
    {MODKEY,                           XK_k,                     focusstack,     {.i = -1}               },
    {MODKEY | ShiftMask,               XK_j,                     movestack,      {.i = +1}               },
    {MODKEY | ShiftMask,               XK_k,                     movestack,      {.i = -1}               },
    {MODKEY,                           XK_o,                     incnmaster,     {.i = +1}               },
    {MODKEY | ShiftMask,               XK_o,                     incnmaster,     {.i = -1}               },
    {MODKEY,                           XK_h,                     setmfact,       {.f = -0.05}            },
    {MODKEY,                           XK_l,                     setmfact,       {.f = +0.05}            },
    {MODKEY,                           XK_f,                     fullscreen,     {0}                     },
    {MODKEY,                           XK_z,                     zoom,           {0}                     },
    {MODKEY,                           XK_Tab,                   spawn,          {.v = rofiswitchcmd}    },
    {MODKEY | ShiftMask,               XK_q,                     killclient,     {0}                     },
    {MODKEY | ShiftMask,               XK_x,                     spawn,          {.v = xkillcmd}         },
    {MODKEY | ShiftMask,               XK_s,                     spawn,          SHCMD(screenshotcmd)    },
    {MODKEY,                           XK_s,                     setlayout,      {.v = &layouts[1]}      },
    {MODKEY,                           XK_w,                     setlayout,      {.v = &layouts[3]}      },
    {MODKEY,                           XK_e,                     setlayout,      {.v = &layouts[0]}      },
    {MODKEY,                           XK_space,                 setlayout,      {.v = &layouts[4]}      },
    {MODKEY | ControlMask,             XK_space,                 spawn,          SHCMD(dunstcloseallcmd) },
    {MODKEY,                           XK_n,                     spawn,          SHCMD(dunstpausecmd)    },
    {MODKEY | ShiftMask,               XK_space,                 togglefloating, {0}                     },
    {MODKEY | ControlMask,		         XK_backslash,             cyclelayout,    {.i = +1}               },
    {MODKEY,                           XK_equal,                 view,           {.ui = ~0}              },
    {MODKEY | ControlMask,             XK_equal,                 tag,            {.ui = ~0}              },
    {MODKEY,                           XK_Down,                  moveresize,     {.v = "0x 25y 0w 0h"}   },
    {MODKEY,                           XK_Up,                    moveresize,     {.v = "0x -25y 0w 0h"}  },
    {MODKEY,                           XK_Right,                 moveresize,     {.v = "25x 0y 0w 0h"}   },
    {MODKEY,                           XK_Left,                  moveresize,     {.v = "-25x 0y 0w 0h"}  },
    {MODKEY | ShiftMask,               XK_Down,                  moveresize,     {.v = "0x 0y 0w 25h"}   },
    {MODKEY | ShiftMask,               XK_Up,                    moveresize,     {.v = "0x 0y 0w -25h"}  },
    {MODKEY | ShiftMask,               XK_Right,                 moveresize,     {.v = "0x 0y 25w 0h"}   },
    {MODKEY | ShiftMask,               XK_Left,                  moveresize,     {.v = "0x 0y -25w 0h"}  },
    {MODKEY | ControlMask,             XK_Up,                    moveresizeedge, {.v = "t"}              },
    {MODKEY | ControlMask,             XK_Down,                  moveresizeedge, {.v = "b"}              },
    {MODKEY | ControlMask,             XK_Left,                  moveresizeedge, {.v = "l"}              },
    {MODKEY | ControlMask,             XK_Right,                 moveresizeedge, {.v = "r"}              },
    {MODKEY | ControlMask | ShiftMask, XK_Up,                    moveresizeedge, {.v = "T"}              },
    {MODKEY | ControlMask | ShiftMask, XK_Down,                  moveresizeedge, {.v = "B"}              },
    {MODKEY | ControlMask | ShiftMask, XK_Left,                  moveresizeedge, {.v = "L"}              },
    {MODKEY | ControlMask | ShiftMask, XK_Right,                 moveresizeedge, {.v = "R"}              },
    {MODKEY | ShiftMask,               XK_e,                     quit,           {0}                     },
    {MODKEY | ShiftMask,               XK_r,                     quit,           {1}                     },
    TAGKEYS(XK_1, 0) TAGKEYS(XK_2, 1) TAGKEYS(XK_3, 2) TAGKEYS(XK_4, 3) TAGKEYS(XK_5, 4) 
    TAGKEYS(XK_6, 5) TAGKEYS(XK_7, 6) TAGKEYS(XK_8, 7) TAGKEYS(XK_9, 8)
};

static const Button buttons[] = {
    {ClkLtSymbol,   0,      Button1, setlayout,      {0}                },
    {ClkLtSymbol,   0,      Button3, setlayout,      {.v = &layouts[2]} },
    {ClkStatusText, 0,      Button2, spawn,          {.v = termcmd}     },
    {ClkClientWin,  MODKEY, Button1, moveorplace,    {0}                },
    {ClkClientWin,  MODKEY, Button2, togglefloating, {0}                },
    {ClkClientWin,  MODKEY, Button3, resizemouse,    {0}                },
    {ClkTagBar,     0,      Button1, view,           {0}                },
    {ClkTagBar,     0,      Button3, toggleview,     {0}                },
    {ClkTagBar,     MODKEY, Button1, tag,            {0}                },
    {ClkTagBar,     MODKEY, Button3, toggletag,      {0}                },
};
